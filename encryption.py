'''
CSC 411 SYMETRIC ENCRYPTION
ISAAC KIPTOO MULWA
P15/37217/2016
'''

import base64, os
from Crypto.Cipher import AES
import decryption


def generate_encryption_key():
    # the length of AES encryotion key(multiples of 16(16/32/64))
    encryption_key_length = 16  # use larger value in production
    # generate a random encryption key with the encryption key
    encryption_key = os.urandom(encryption_key_length)
    # the encryption key is encoded for safe storage
    encoded_encryption_key = base64.b64encode(encryption_key)
    return encoded_encryption_key


def encrypt_string(private_message, encoded_encryption_key, padding_character):
    # decode the encoded encryption key using base64 encoding
    encryption_key = base64.b64decode(encoded_encryption_key)
    # use the decoded encryption key to create an AES cipher
    cipher = AES.new(encryption_key, AES.MODE_EAX)
    # pad the private_message to make it a multiple of 16
    padded_private_message = private_message + (padding_character * ((16 - len(private_message)) % 16))
    # use the cipher to encrypt the padded string
    encrypted_msg = cipher.encrypt(padded_private_message)
    # encode the encrypted msg for safe storage
    encoded_encrypted_msg = base64.b64encode(encrypted_msg)
    # return the encoded encrypted string
    return encoded_encrypted_msg


private_message = input("Enter the string you want to encrypt: ")
padding_character = "n"

encryption_key = generate_encryption_key()
encrypted_msg = encrypt_string(private_message, encryption_key, padding_character)
decrypted_msg = decryption.decrypt_string(encrypted_msg, encryption_key, padding_character)

print("Encryption Key: %s - (%d)" % (encryption_key, len(encryption_key)))
print("Encrypted String: %s - (%d)" % (encrypted_msg, len(encrypted_msg)))
print("Decrypted String: %s - (%d)" % (decrypted_msg, len(decrypted_msg)))
