from Crypto.Cipher import AES
import base64, os

def decrypt_string(encoded_encrypted_msg, encoded_encryption_key, padding_character):
    # decode the encoded encrypted string and encoded encryption key
    encryption_key = base64.b64decode(encoded_encryption_key)
    encrypted_msg = base64.b64decode(encoded_encrypted_msg)
    # use the decoded encryption key to create a AES cipher
    cipher = AES.new(encryption_key, AES.MODE_EAX)
    # use the cipher to decrypt the encrypted string
    decrypted_msg = cipher.decrypt(encrypted_msg)
    # unpad the encrypted string
    unpadded_private_message = decrypted_msg.rstrip(padding_character)
    # return a decrypted original private string
    return unpadded_private_message